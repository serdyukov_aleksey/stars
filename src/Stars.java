import javax.print.attribute.standard.Sides;
import java.util.Arrays;

public class Stars {
    private final static int SIDE = 10;
    private int taskCount;

    Stars() {
        printArray(task1());
        printArray(task2());
        printArray(task3());
        printArray(task4());
        printArray(task5());
        printArray(task6());
        printArray(task7());
        printArray(task8());
        printArray(task9());
        printArray(task10());
        printArray(task11());

    }

    private void printArray (String[][] array){

        for (int i = 0; i < SIDE; i++) {
            for (int j = 0; j < SIDE; j++) {
                System.out.print(array[i][j]==null?"  ":array[i][j]);
            }
            System.out.println(" ");
        }
    }

    private String [][] task1() {
        printTask(++taskCount);
        int iteration =0;
        String [][] array = new String[SIDE][SIDE];

        for (int i = 0; i < SIDE; i++) {
            array[i][i]="* ";
            for (int j = 0; j < i; j++) {
                array[i][j]=array[j][i]="* ";
                iteration++;
            }
        }
        System.out.println("Iterations: "+iteration);
        return array;
    }


    private String [][] task2() {
        printTask(++taskCount);
        int iteration =0;
        String [][] array = new String[SIDE][SIDE];
        for (int i = 0; i < SIDE; i++) {
            array[i][0]="* ";
            array[0][i]="* ";
            array[i][SIDE-1]="* ";
            array[SIDE-1][SIDE-i-1]="* ";
            iteration++;
        }

        System.out.println("Iterations: "+iteration);
        return array;
    }

    private String [][] task3() {
        printTask(++taskCount);
        int iteration =0;
        String [][] array = new String[SIDE][SIDE];
        for (int i = 0; i < SIDE; i++) {
            array[i][0]="* ";
            array[0][i]="* ";
            array[i][SIDE-i-1]="* ";
            iteration++;
        }

        System.out.println("Iterations: "+iteration);
        return array;
    }

    private String [][] task4() {
        printTask(++taskCount);
        int iteration =0;
        String [][] array = new String[SIDE][SIDE];
        for (int i = 0; i < SIDE; i++) {
            array[i][0]="* ";
            array[i][i]="* ";
            array[SIDE-1][i]="* ";
            iteration++;
        }

        System.out.println("Iterations: "+iteration);
        return array;
    }


    private String [][] task5() {
        printTask(++taskCount);
        int iteration =0;
        String [][] array = new String[SIDE][SIDE];
        for (int i = 0; i < SIDE; i++) {
            array[i][SIDE-1]="* ";
            array[i][SIDE-1-i]="* ";
            array[SIDE-1][i]="* ";
            iteration++;
        }

        System.out.println("Iterations: "+iteration);
        return array;
    }

    private String [][] task6() {
        printTask(++taskCount);
        int iteration =0;
        String [][] array = new String[SIDE][SIDE];
        for (int i = 0; i < SIDE; i++) {
            array[i][SIDE-1]="* ";
            array[i][i]="* ";
            array[0][i]="* ";
            iteration++;
        }

        System.out.println("Iterations: "+iteration);
        return array;
    }


    private String [][] task7() {
        printTask(++taskCount);
        int iteration =0;
        String [][] array = new String[SIDE][SIDE];
        for (int i = 0; i < SIDE; i++) {
            array[i][i]="* ";
            array[SIDE-1-i][i]="* ";
//            array[i][SIDE-1]="* ";
//            array[0][i]="* ";
            iteration++;
        }

        System.out.println("Iterations: "+iteration);
        return array;
    }

    private String [][] task8() {
        printTask(++taskCount);
        int iteration =0;
        String [][] array = new String[SIDE][SIDE];
        for (int i = 0; i < SIDE; i++) {
            if(i<=SIDE/2){array[i][i]="* ";}
            if(i<=SIDE/2){array[i][SIDE-1-i]="* ";}
            array[0][i]="* ";
//            array[0][i]="* ";
            iteration++;
        }

        System.out.println("Iterations: "+iteration);
        return array;
    }

    private String [][] task9() {
        printTask(++taskCount);
        int iteration =0;
        String [][] array = new String[SIDE][SIDE];
        for (int i = 0; i < SIDE; i++) {
            if(i>=SIDE/2){array[i][i]="* ";}
            if(i>=SIDE/2){array[i][SIDE-1-i]="* ";}
            array[SIDE-1][i]="* ";
//            array[0][i]="* ";
            iteration++;
        }

        System.out.println("Iterations: "+iteration);
        return array;
    }

    private String [][] task10() {
        printTask(++taskCount);
        int iteration =0;
        String [][] array = new String[SIDE][SIDE];
        for (int i = 0; i < SIDE; i++) {
            if(i>=SIDE/2){array[i][i]="* ";}
            if(i<=SIDE/2){array[i][SIDE-1-i]="* ";}
            array[i][SIDE-1]="* ";
            iteration++;
        }

        System.out.println("Iterations: "+iteration);
        return array;
    }

    private String [][] task11() {
        printTask(++taskCount);
        int iteration =0;
        String [][] array = new String[SIDE][SIDE];
        for (int i = 0; i < SIDE; i++) {
            if(i<=SIDE/2){array[i][i]="* ";}
            if(i>=SIDE/2){array[i][SIDE-1-i]="* ";}
            array[i][0]="* ";
            iteration++;
        }

        System.out.println("Iterations: "+iteration);
        return array;
    }

    private void printTask(int number) {
        System.out.println("\nTASK " + number);
    }

    public static void main(String[] args) {
        new Stars();
    }

}


